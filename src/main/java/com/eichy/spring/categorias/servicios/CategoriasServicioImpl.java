package com.eichy.spring.categorias.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eichy.spring.categorias.beans.CategoriasBean;
import com.eichy.spring.categorias.daos.CategoriasDAO;
import com.eichy.spring.sistema.beans.MensajeTransaccionBean;
import com.eichy.spring.sistema.servicios.BaseServicio;

@Service
public class CategoriasServicioImpl extends BaseServicio implements CategoriasServicio{

	private interface Consulta{
		int	categoriasActivas	=	1;
		int categoria			=	2;
	}
	
	private interface Operaciones{
		int altaCategoria		=	1;
		int actualizaCategoria	=	2;
	}
	
	@Autowired
	CategoriasDAO categoriasDAO;
	
	@Override
	public List<CategoriasBean> lista(CategoriasBean categoriasBean, int tipoConsulta) {
		List<CategoriasBean> categorias = null;

		switch (tipoConsulta) {
		case Consulta.categoriasActivas:
			categorias = categoriasDAO.categoriasActivas(categoriasBean);
			break;
		default:
			break;
		}
		
		return categorias;
	}

	@Override
	public CategoriasBean consulta(CategoriasBean categoriasBean, int tipoConsulta) {
		
		switch (tipoConsulta) {
		case Consulta.categoria:
			categoriasBean = categoriasDAO.consultaCategoria(categoriasBean);
			break;

		default:
			break;
		}
		
		return categoriasBean;
	}

	@Override
	public MensajeTransaccionBean grabaTransaccion(CategoriasBean categoriasBean, int tipoOperacion) {
		MensajeTransaccionBean mensajeTransaccionBean = null;
		
		switch (tipoOperacion) {
		case Operaciones.altaCategoria:
			mensajeTransaccionBean = categoriasDAO.altaCategorias(categoriasBean);
			break;
		case Operaciones.actualizaCategoria:
			mensajeTransaccionBean = categoriasDAO.actualizaCategorias(categoriasBean);
			break;

		default:
			break;
		}
		
		return mensajeTransaccionBean;
	}

}
