package com.eichy.spring.categorias.servicios;

import java.util.List;

import com.eichy.spring.categorias.beans.CategoriasBean;
import com.eichy.spring.sistema.beans.MensajeTransaccionBean;

public interface CategoriasServicio {
	public	List<CategoriasBean>  lista(CategoriasBean categoriasBean, int tipoConsulta);
	public	CategoriasBean	consulta(CategoriasBean categoriasBean, int tipoConsulta);
	public	MensajeTransaccionBean grabaTransaccion(CategoriasBean categoriasBean, int tipoOperacion);
}
