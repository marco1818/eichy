package com.eichy.spring.categorias.beans;

import java.math.BigDecimal;

import com.eichy.spring.sistema.beans.BaseBean;

public class CategoriasBean  extends BaseBean{
	private	int	categoriaId;
	private String nombre;
	private int estatus;
	
	public CategoriasBean() {
		
	}

	public int getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(int categoriaId) {
		this.categoriaId = categoriaId;
	}

	public void setCategoriaId(BigDecimal categoriaId) {
		this.categoriaId = categoriaId.intValue();
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEstatus() {
		return estatus;
	}

	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}
	
	public void setEstatus(BigDecimal estatus) {
		this.estatus = estatus.intValue();
	}
}
