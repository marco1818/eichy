package com.eichy.spring.categorias.daos;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.eichy.spring.categorias.beans.CategoriasBean;
import com.eichy.spring.sistema.beans.MensajeTransaccionBean;
import com.eichy.spring.sistema.daos.BaseDAO;

@Repository
public class CategoriasDAOImpl extends BaseDAO implements CategoriasDAO{

	private	interface Funciones{
		String	altaCategoria		=	"CATEGORIAS_ALTA_CATEGORIAS";
		String 	consultaCategoria	=	"CATEGORIAS_CONSULTA_CATEGORIAS";
		String	actualizaCategoria	=	"CATEGORIAS_ACTUALIZA_CATEGORIAS";
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CategoriasBean> categoriasActivas(CategoriasBean categoriasBean) {
		List<CategoriasBean> categorias = null;
		ArrayList<Object> parametros = new ArrayList<>();
		parametros.add(1);
		parametros.add(0);
		
		categorias = (List<CategoriasBean>) ejecutaFuncionAll(Funciones.consultaCategoria, parametros, CategoriasBean.class);
		
		return categorias;
	}

	@Override
	public CategoriasBean consultaCategoria(CategoriasBean categorias) {
		CategoriasBean categoriasBean = null;
		ArrayList<Object> parametros = new ArrayList<>();
		parametros.add(2);
		parametros.add(categorias.getCategoriaId());
		
		categoriasBean = (CategoriasBean) ejecutaFuncion(Funciones.consultaCategoria, parametros, CategoriasBean.class);
		
		return categoriasBean;
	}
	
	@Override
	public MensajeTransaccionBean altaCategorias(CategoriasBean categoriasBean) {
		MensajeTransaccionBean mensajeTransaccionBean = null;
		ArrayList<Object> parametros = new ArrayList<>();
		parametros.add(categoriasBean.getNombre());
		parametros.add(1);
		parametros.add(1);
		parametros.add(2);
		
		mensajeTransaccionBean = (MensajeTransaccionBean) ejecutaFuncion(Funciones.altaCategoria, parametros, MensajeTransaccionBean.class);
		
		return mensajeTransaccionBean;
	}

	@Override
	public MensajeTransaccionBean actualizaCategorias(CategoriasBean categoriasBean) {
		MensajeTransaccionBean	mensajeTransaccionBean = null;
		ArrayList<Object> parametros = new ArrayList<>();
		parametros.add(categoriasBean.getCategoriaId());
		parametros.add(categoriasBean.getNombre());
		parametros.add(categoriasBean.getEstatus());
		parametros.add(1);
		parametros.add(2);
		
		mensajeTransaccionBean = (MensajeTransaccionBean) ejecutaFuncion(Funciones.actualizaCategoria, parametros, MensajeTransaccionBean.class);
		
		return mensajeTransaccionBean;
	}



}
