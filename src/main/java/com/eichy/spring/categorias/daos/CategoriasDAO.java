package com.eichy.spring.categorias.daos;

import java.util.List;

import com.eichy.spring.categorias.beans.CategoriasBean;
import com.eichy.spring.sistema.beans.MensajeTransaccionBean;

public interface CategoriasDAO {
	public	List<CategoriasBean> categoriasActivas(CategoriasBean categoriasBean);
	public	CategoriasBean consultaCategoria(CategoriasBean categoriasBean);
	public	MensajeTransaccionBean altaCategorias(CategoriasBean categoriasBean);
	public	MensajeTransaccionBean actualizaCategorias(CategoriasBean categoriasBean);
}
