package com.eichy.spring.categorias.controladores;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.eichy.spring.categorias.beans.CategoriasBean;
import com.eichy.spring.categorias.servicios.CategoriasServicio;
import com.eichy.spring.sistema.beans.MensajeTransaccionBean;

@Controller
public class CategoriasControlador {

	@InitBinder
	public void InitBinder(WebDataBinder webDataBinder, WebRequest webRequest) {
		webDataBinder.setDisallowedFields("");
	}	
	
	@Autowired
	CategoriasServicio categoriasServicio;
	
	@RequestMapping(value= {"/categorias.htm"}, method= RequestMethod.GET)
	public	ModelAndView inicio() {
		ModelAndView modelAndView = new ModelAndView();
		List<CategoriasBean> categorias = null;
		
		categorias = categoriasServicio.lista(new CategoriasBean(), 1);
		
		modelAndView.setViewName("categorias/categorias");
		modelAndView.addObject("categorias",categorias);
		
		return modelAndView;
	}
	
	@RequestMapping(value= {"/categoriasTranccion.htm"}, method= RequestMethod.POST)
	public	ModelAndView transacciones(@ModelAttribute("categoriasBean") CategoriasBean categoriasBean, @RequestParam int tipoOperacion) {
		ModelAndView modelAndView = new ModelAndView();
		
		MensajeTransaccionBean mensajeTransaccionBean = null;
		mensajeTransaccionBean = categoriasServicio.grabaTransaccion(categoriasBean, tipoOperacion);
		
		modelAndView.setViewName("sistema/resultadoTransaccion");
		modelAndView.addObject("mensajeTransaccionBean",mensajeTransaccionBean);
		return modelAndView;
	}
	
}
