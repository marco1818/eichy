package com.eichy.spring.puntoventa.controladores;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.eichy.spring.puntoventa.beans.PuntoVentaForm;
import com.eichy.spring.puntoventa.servicios.PuntoVentaServicio;

@Controller
public class PuntoVentaControlador {
	@Autowired
	PuntoVentaServicio puntoVentaServicio;
	
	@InitBinder
	public void InitBinder(WebDataBinder webDataBinder, WebRequest webRequest) {
		webDataBinder.setDisallowedFields("");
	}	
	
	@RequestMapping(value= {"/puntoVenta.htm"}, method = RequestMethod.GET)
	public	ModelAndView	inicio() {
		ModelAndView modelAndView = new ModelAndView();
		PuntoVentaForm puntoVentaForm = new PuntoVentaForm();
		puntoVentaForm = puntoVentaServicio.consulta(puntoVentaForm, 1);
		modelAndView.addObject("puntoVentaForm",puntoVentaForm);
		modelAndView.setViewName("puntoVenta/puntoVenta");
		return modelAndView;
	}
}
