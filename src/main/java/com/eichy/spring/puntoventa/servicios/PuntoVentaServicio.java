package com.eichy.spring.puntoventa.servicios;

import com.eichy.spring.puntoventa.beans.PuntoVentaForm;
import com.eichy.spring.sistema.beans.MensajeTransaccionBean;

public interface PuntoVentaServicio {
	public	PuntoVentaForm	consulta(PuntoVentaForm puntoVentaForm, int tipoConsulta);
	public	MensajeTransaccionBean	grabaTransaccion(PuntoVentaForm	puntoVentaForm, int tipoOperacion);
}
