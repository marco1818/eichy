package com.eichy.spring.puntoventa.servicios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eichy.spring.categorias.servicios.CategoriasServicio;
import com.eichy.spring.productos.servicios.ProductosServicio;
import com.eichy.spring.puntoventa.beans.PuntoVentaForm;
import com.eichy.spring.sistema.beans.MensajeTransaccionBean;
import com.eichy.spring.sistema.servicios.BaseServicio;

@Service
public class PuntoVentaServicioImpl extends BaseServicio implements PuntoVentaServicio{

	private	interface	Consultas{
		int	categorias	=	1;
		int	productos	=	2;
	}
	
	@Autowired
	CategoriasServicio categoriasServicio;
	
	@Autowired
	ProductosServicio productosServicio;
	
	@Override
	public PuntoVentaForm consulta(PuntoVentaForm puntoVentaForm, int tipoConsulta) {
		
		switch (tipoConsulta) {
			case Consultas.categorias:
				puntoVentaForm.setCategorias(categoriasServicio.lista(null, 1));
				break;
			case Consultas.productos:
				puntoVentaForm.setProductos(productosServicio.consulta(null, tipoConsulta).getProductos());
				break;
	
			default:
				break;
		}
		
		return puntoVentaForm;
	}

	@Override
	public MensajeTransaccionBean grabaTransaccion(PuntoVentaForm puntoVentaForm, int tipoOperacion) {
		// TODO Auto-generated method stub
		return null;
	}

}
