package com.eichy.spring.puntoventa.beans;

import java.util.List;

import com.eichy.spring.categorias.beans.CategoriasBean;
import com.eichy.spring.productos.beans.InsumosxProductoBean;
import com.eichy.spring.productos.beans.ProductosBean;

public class PuntoVentaForm {
	private	List<CategoriasBean>	categorias;
	private	List<ProductosBean>		productos;
	private	List<InsumosxProductoBean>	insumosxProductoBeans;
	
	public PuntoVentaForm(){
		
	}

	public List<CategoriasBean> getCategorias() {
		return categorias;
	}

	public void setCategorias(List<CategoriasBean> categorias) {
		this.categorias = categorias;
	}

	public List<ProductosBean> getProductos() {
		return productos;
	}

	public void setProductos(List<ProductosBean> productos) {
		this.productos = productos;
	}

	public List<InsumosxProductoBean> getInsumosxProductoBeans() {
		return insumosxProductoBeans;
	}

	public void setInsumosxProductoBeans(List<InsumosxProductoBean> insumosxProductoBeans) {
		this.insumosxProductoBeans = insumosxProductoBeans;
	}
	
	
}
