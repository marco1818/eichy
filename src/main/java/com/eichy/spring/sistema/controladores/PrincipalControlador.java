package com.eichy.spring.sistema.controladores;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.eichy.spring.insumos.InsumosBean;
import com.eichy.spring.insumos.servicios.InsumosServicio;
import com.eichy.spring.sistema.beans.MensajeTransaccionBean;

@Controller
public class PrincipalControlador {

	@InitBinder
	public void InitBinder(WebDataBinder webDataBinder, WebRequest webRequest) {
		webDataBinder.setDisallowedFields("");
	}	
	
	@Autowired
	InsumosServicio insumosServicio;
	
	@RequestMapping(value= {"/principal.htm"}, method=RequestMethod.GET)
	public ModelAndView principal() {
		ModelAndView modelAndView = new ModelAndView();
		List<InsumosBean> insumos = null;
		
		insumos = insumosServicio.consultaLista(new InsumosBean(), 1);
		
		modelAndView.setViewName("sistema/principal");		
		modelAndView.addObject("insumosLista", insumos);
		
		return modelAndView;
	}	
	@RequestMapping(value= {"/insumos1.htm"}, method=RequestMethod.POST)
	public	ModelAndView altaInsumos(@ModelAttribute("insumosBean") InsumosBean insumosBean, @RequestParam int tipoOperacion) {
		ModelAndView modelAndView = new ModelAndView();
		MensajeTransaccionBean mensajeTransaccionBean = null;
		
		mensajeTransaccionBean = insumosServicio.grabaTransaccion(insumosBean, tipoOperacion);
		
		modelAndView.setViewName("sistema/resultadoTransaccion");
		modelAndView.addObject("mensajeTransaccionBean",mensajeTransaccionBean);
		
		return modelAndView;
	}
}
