package com.eichy.spring.sistema.servicios;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.eichy.spring.sistema.beans.MensajeTransaccionBean;
import com.eichy.spring.sistema.daos.UtileriasDAO;

@Component
public class BaseServicio {
	
	@Autowired
	UtileriasDAO utileriasDAO;
	
	public	BigDecimal	numeroTransaccion() {
		MensajeTransaccionBean mensajeTransaccionBean = utileriasDAO.numeroTransaccion();
		
		return new BigDecimal(mensajeTransaccionBean.getNumeroMensaje()); 
	}	
	

}
