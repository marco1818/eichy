package com.eichy.spring.sistema.servicios;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.yaml.snakeyaml.Yaml;

import com.eichy.spring.sistema.beans.FuncionBean;
import com.eichy.spring.sistema.beans.YamlUtilsBean;
import com.eichy.spring.sistema.daos.BaseDAO;

public class FuncionesSistemaServicio {
	
	private final static Logger logger = Logger.getLogger("FuncionesSistema");
	private static FuncionesSistemaServicio funcionesSistema;
	private Map<String,FuncionBean> mapFuncionesSistema;
	
	private FuncionesSistemaServicio() {
		mapFuncionesSistema	=	new HashMap<String,FuncionBean>();
	}
	
	public static FuncionesSistemaServicio getInstance() {
		if(funcionesSistema == null) {
			funcionesSistema = new FuncionesSistemaServicio();
			funcionesSistema.cargaFunciones();
		}
		return funcionesSistema;
	}
	
	private void cargaFunciones() {
		Yaml yaml = new Yaml();
		try {
			logger.info("Cargando Funciones del Sistema.");
			
			List<InputStream> recursos = new ArrayList<InputStream>(); 
			recursos.add(BaseDAO.class.getResourceAsStream("funciones/insumos.yml"));
			recursos.add(BaseDAO.class.getResourceAsStream("funciones/utilerias.yml"));
			recursos.add(BaseDAO.class.getResourceAsStream("funciones/categorias.yml"));
			recursos.add(BaseDAO.class.getResourceAsStream("funciones/productos.yml"));
			for(InputStream recurso: recursos) {
				YamlUtilsBean yamlUtils = yaml.loadAs(recurso, YamlUtilsBean.class);
				for (FuncionBean funcionBean : yamlUtils.getFunciones()) {
					mapFuncionesSistema.put(funcionBean.getLlaveFuncion().trim(), funcionBean);
					logger.info("Funcion:"+funcionBean.getLlaveFuncion().trim()+"-Cargada Exitosamente.");
				}
		  
				recurso.close();
			}
			
			logger.info("Funciones del Sistema Cargadas Exitosamente.");
		} catch (IOException e) {
			logger.info("Ocurrio un detalle al Cargar la Funciones del Sistema.");
		}
	}
	
	public FuncionBean obtenerFuncion(String clave) {
		return mapFuncionesSistema.get(clave);
	}
}
