package com.eichy.spring.sistema.beans;

import java.util.Map;

public class FuncionBean {
	private String creado;
	private String descripcion;
	private String controladores;
	private String ultimaModificacion;
	private String descripcionCambio;
	private String schema;
	private String nombreFuncion;
	private String llaveFuncion;
	private Map<String, String> resultado;
	
	public FuncionBean() {
		
	}

	public String getCreado() {
		return creado;
	}

	public void setCreado(String creado) {
		this.creado = creado;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getControladores() {
		return controladores;
	}

	public void setControladores(String controladores) {
		this.controladores = controladores;
	}

	public String getUltimaModificacion() {
		return ultimaModificacion;
	}

	public void setUltimaModificacion(String ultimaModificacion) {
		this.ultimaModificacion = ultimaModificacion;
	}

	public String getDescripcionCambio() {
		return descripcionCambio;
	}

	public void setDescripcionCambio(String descripcionCambio) {
		this.descripcionCambio = descripcionCambio;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public String getNombreFuncion() {
		return nombreFuncion;
	}

	public void setNombreFuncion(String nombreFuncion) {
		this.nombreFuncion = nombreFuncion;
	}

	public String getLlaveFuncion() {
		return llaveFuncion;
	}

	public void setLlaveFuncion(String llaveFuncion) {
		this.llaveFuncion = llaveFuncion;
	}

	public Map<String, String> getResultado() {
		return resultado;
	}

	public void setResultado(Map<String, String> resultado) {
		this.resultado = resultado;
	}
}
