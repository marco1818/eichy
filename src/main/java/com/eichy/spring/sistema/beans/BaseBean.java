package com.eichy.spring.sistema.beans;

import java.math.BigDecimal;
import java.security.SecureRandom;

public class BaseBean {
	private	int	usuarioId;
	private BigDecimal numeroTransaccion;
	private SecureRandom versionador = new SecureRandom();
	private	double	version;
	
	public BaseBean() {
		version = versionador.nextDouble();
	}
	
	public int getUsuarioId() {
		return usuarioId;
	}
	
	public void setUsuarioId(int usuarioId) {
		this.usuarioId = usuarioId;
	}
	
	public void setUsuarioId(BigDecimal usuarioId) {
		this.usuarioId = usuarioId.intValue();
	}
	
	public BigDecimal getNumeroTransaccion() {
		return numeroTransaccion;
	}
	
	public void setNumeroTransaccion(BigDecimal numeroTransaccion) {
		this.numeroTransaccion = numeroTransaccion;
	}

	public double getVersion() {
		return version;
	}

	public void setVersion(double version) {
		this.version = version;
	}
}
