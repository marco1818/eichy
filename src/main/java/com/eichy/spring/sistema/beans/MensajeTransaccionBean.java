package com.eichy.spring.sistema.beans;

import java.math.BigDecimal;

public class MensajeTransaccionBean {
	private	int	numeroMensaje;
	private	int	mensajeId;
	private	String mensaje;
	
	public MensajeTransaccionBean() {
		
	}

	public int getNumeroMensaje() {
		return numeroMensaje;
	}

	public void setNumeroMensaje(int numeroMensaje) {
		this.numeroMensaje = numeroMensaje;
	}

	public void setNumeroMensaje(BigDecimal numeroMensaje) {
		this.numeroMensaje = numeroMensaje.intValue();
	}
	
	public int getMensajeId() {
		return mensajeId;
	}

	public void setMensajeId(int mensajeId) {
		this.mensajeId = mensajeId;
	}

	public void setMensajeId(BigDecimal mensajeId) {
		this.mensajeId = mensajeId.intValue();
	}
	
	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
}
