package com.eichy.spring.sistema.beans;

import java.util.List;

public class YamlUtilsBean {
	List<FuncionBean> funciones;

	public List<FuncionBean> getFunciones() {
		return funciones;
	}

	public void setFunciones(List<FuncionBean> funciones) {
		this.funciones = funciones;
	}
}
