package com.eichy.spring.sistema.daos;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.CallableStatementCallback;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.eichy.spring.sistema.beans.FuncionBean;
import com.eichy.spring.sistema.servicios.FuncionesSistemaServicio;

@Component
public class BaseDAO {
	private final static Logger logger = Logger.getLogger("BaseDAO");
	
	@Autowired
	protected JdbcTemplate jdbcTemplate;
	
	public BaseDAO() {
	
	}

	public BaseDAO(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	public List<? extends Object> ejecutaFuncionAll(String nombreFuncion,final ArrayList<? extends Object> parametros,final Class<?> clase){
		FuncionesSistemaServicio funcionesSistema	=	FuncionesSistemaServicio.getInstance();
		List<Object> resultado = new ArrayList<Object>();
		final FuncionBean funcion = funcionesSistema.obtenerFuncion(nombreFuncion);
		logger.info("CALL "+funcion.getSchema()+"."+funcion.getNombreFuncion().replaceAll("\\?", "").replaceAll("\\(", "").replaceAll("\\)", "").replaceAll(",", "")+"("+Arrays.toString(parametros.toArray())+")");
		
		resultado = jdbcTemplate.execute(new CallableStatementCreator() {
			
			@Override
			public CallableStatement createCallableStatement(Connection con) throws SQLException {
				CallableStatement callableStatement	=	con.prepareCall("{? = call "+funcion.getSchema()+"."+funcion.getNombreFuncion()+"}");
				return cargaParametros(callableStatement,parametros);
			}
		}, new CallableStatementCallback<List<Object>>() {

			@Override
			public List<Object> doInCallableStatement(CallableStatement cs) throws SQLException, DataAccessException {
				cs.execute();
				ResultSet resultSet = (ResultSet) cs.getObject(1);
				
				return obtenerresultadoAll(resultSet, funcion, clase, parametros);
			}
		});
		
		
		return resultado;
	}
	
	public Object ejecutaFuncion(String nombreFuncion,final ArrayList<? extends Object> parametros,final Class<?> clase){
		FuncionesSistemaServicio funcionesSistema	=	FuncionesSistemaServicio.getInstance();
		Object resultado = new Object();
		final FuncionBean funcion = funcionesSistema.obtenerFuncion(nombreFuncion);
		logger.info("CALL "+funcion.getSchema()+"."+funcion.getNombreFuncion().replaceAll("\\?", "").replaceAll("\\(", "").replaceAll("\\)", "").replaceAll(",", "")+"("+Arrays.toString(parametros.toArray())+")");
		
		resultado = jdbcTemplate.execute(new CallableStatementCreator() {
			
			@Override
			public CallableStatement createCallableStatement(Connection con) throws SQLException {
				CallableStatement callableStatement	=	con.prepareCall("{? = call "+funcion.getSchema()+"."+funcion.getNombreFuncion()+"}");
				return cargaParametros(callableStatement,parametros);
			}
		}, new CallableStatementCallback<Object>() {

			@Override
			public Object doInCallableStatement(CallableStatement cs) throws SQLException, DataAccessException {
				cs.execute();
				ResultSet resultSet = (ResultSet) cs.getObject(1);

				return obtenerresultado(resultSet, funcion, clase, parametros);
			}
		});
		
		
		return resultado;
	}
	
	public CallableStatement cargaParametros(CallableStatement callableStatement,ArrayList<? extends Object> parametros) {
		int posicion = 2;
		try {
			callableStatement.registerOutParameter(1, -10);

			for(Object parametro : parametros) {
				
				callableStatement.setObject(posicion, parametro);
				posicion++;
			}
		}catch (SQLException e) {
			logger.info("Ocurrio un problema en la Carga de Parametros.");
		 }
		return callableStatement;
	}
	
	private List<Object> obtenerresultadoAll(ResultSet resultSet, FuncionBean funcion, Class<?> clase,ArrayList<? extends Object> parametros){
		List<Object> resultado = new ArrayList<Object>();
		Map<String,Method> metodos = new HashMap<String,Method>();
		try {
			metodos = cargaMetodos(clase);
			while(resultSet.next()) {
				Object objeto = clase.newInstance();
				Iterator<Entry<String, String>> iterator = funcion.getResultado().entrySet().iterator();
				while(iterator.hasNext()) {
					
					Map.Entry<String, String> salida = (Map.Entry<String, String>)iterator.next();
					Method metodo = metodos.get(salida.getKey().toUpperCase());
					Class<?>[] tipoDato = metodo.getParameterTypes();
										
					if(tipoDato[0].getName().contains("java.lang.String")) {
						String valor = resultSet.getString(salida.getValue());
						metodo.invoke(objeto,valor);
						continue;
					}
					
					if(tipoDato[0].getName().contains("java.math.BigDecimal")){
						String valorSRT;
						BigDecimal valor;
						try {
							valorSRT=resultSet.getObject(String.valueOf(salida.getValue())).toString().trim();
							valor = new BigDecimal(valorSRT);
						}catch (Exception e) {
							valor = new BigDecimal(0);
						}
						
						metodo.invoke(objeto, valor);
						continue;
					}
					
					if(tipoDato[0].getName().contains("long")){
						String valorSRT=resultSet.getObject(String.valueOf(salida.getValue())).toString().trim();
						Long valor;
						try {
							valor = Long.valueOf(valorSRT);	
						}catch (Exception e) {
							valor = (long) 0;
						}
						
						metodo.invoke(objeto, valor);
						continue;
					}
					
					if(tipoDato[0].getName().contains("java.sql.Timestamp")){
						String valorSRT=resultSet.getObject(String.valueOf(salida.getValue())).toString().trim();
						metodo.invoke(objeto, valorSRT);
						continue;
					}
					
				}
				resultado.add(objeto);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			logger.info("Ocurrio un problema al obtener el resulatdo.REF_1");
		} catch (InstantiationException e) {
			logger.info("Ocurrio un problema al Instanciar el Resultado.REF_2");
		} catch (IllegalAccessException e) {
			logger.info("Ocurrio un problema al Instanciar el Resultado.REF_3");
		} catch (IllegalArgumentException e) {
			logger.info("Ocurrio un problema al Ejecutar la Operacion.REF_4");
		} catch (InvocationTargetException e) {
			logger.info("Ocurrio un problema al Ejecutar la Operacion.REF_5");
		}
		
		return resultado;
	}
	
	private Object obtenerresultado(ResultSet resultSet, FuncionBean funcion, Class<?> clase,ArrayList<? extends Object> parametros){
		Object objeto = null;
		Map<String,Method> metodos = new HashMap<String,Method>();
		try {
			objeto = clase.newInstance();
			metodos = cargaMetodos(clase);
			while(resultSet.next()) {
				Iterator<Entry<String, String>> iterator = funcion.getResultado().entrySet().iterator();
				while(iterator.hasNext()) {
					Map.Entry<String, String> salida = (Map.Entry<String, String>)iterator.next();
					
					Method metodo = metodos.get(salida.getKey().toUpperCase());
					Class<?>[] tipoDato = metodo.getParameterTypes();
					if(tipoDato[0].getName().contains("java.lang.String")) {
						String valor = resultSet.getString(salida.getValue());
						metodo.invoke(objeto,valor);
					}
					
					if(tipoDato[0].getName().contains("java.math.BigDecimal")){
						String valorSRT=resultSet.getObject(String.valueOf(salida.getValue())).toString().trim();
						BigDecimal valor = new BigDecimal(valorSRT);
						metodo.invoke(objeto, valor);
					}
					
					if(tipoDato[0].getName().contains("long")){
						String valorSRT=resultSet.getObject(String.valueOf(salida.getValue())).toString().trim();
						metodo.invoke(objeto,Long.valueOf(valorSRT));
					}
					
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			logger.info("Ocurrio un problema al obtener el resulatdo.REF_1");
		} catch (InstantiationException e) {
			logger.info("Ocurrio un problema al Instanciar el Resultado.REF_2");
		} catch (IllegalAccessException e) {
			logger.info("Ocurrio un problema al Instanciar el Resultado.REF_3");
		} catch (IllegalArgumentException e) {
			logger.info("Ocurrio un problema al Ejecutar la Operacion.REF_4");
		} catch (InvocationTargetException e) {
			logger.info("Ocurrio un problema al Ejecutar la Operacion.REF_5");
		}
		
		return objeto;
	}
	
	public Map<String,Method> cargaMetodos(Class<?> clase){
		Map<String,Method> metodos = new HashMap<String,Method>();
		Method[] metodosClase = clase.getMethods();
		
		for(Method metodo : metodosClase) {
			if(metodo.getName().startsWith("set")) {
				Class<?>[] tipoDato = metodo.getParameterTypes();
				if(!tipoDato[0].getName().contains("int") && !tipoDato[0].getName().contains("double")) {
					metodos.put(metodo.getName().replace("set","").toUpperCase(),  metodo);
				}
			}
		}
		
		return metodos;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
}
