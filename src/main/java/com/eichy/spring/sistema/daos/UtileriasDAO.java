package com.eichy.spring.sistema.daos;

import java.util.ArrayList;

import org.springframework.stereotype.Repository;

import com.eichy.spring.sistema.beans.MensajeTransaccionBean;

@Repository
public class UtileriasDAO extends BaseDAO{

	public	MensajeTransaccionBean	numeroTransaccion() {
		MensajeTransaccionBean mensajeTransaccionBean = null;
		ArrayList<Object> parametros = new  ArrayList<>();
		parametros.add(2);
		parametros.add("TRANSACCIONES");
		parametros.add(0);
		parametros.add("");
		
		mensajeTransaccionBean = (MensajeTransaccionBean) ejecutaFuncion("UTILERIAS_OBTENER_NUMERO_TRANSACCION", parametros, MensajeTransaccionBean.class);
		
		return mensajeTransaccionBean;
	}
}
