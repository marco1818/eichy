package com.eichy.spring.productos.servicios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eichy.spring.categorias.servicios.CategoriasServicio;
import com.eichy.spring.insumos.servicios.InsumosServicio;
import com.eichy.spring.productos.beans.InsumosxProductoBean;
import com.eichy.spring.productos.beans.ProductosBean;
import com.eichy.spring.productos.beans.ProductosForm;
import com.eichy.spring.productos.daos.ProductosDAO;
import com.eichy.spring.sistema.beans.MensajeTransaccionBean;
import com.eichy.spring.sistema.servicios.BaseServicio;

@Service
public class ProductosServicioImpl extends BaseServicio implements ProductosServicio{

	private interface	Consultas{
		int listaProductos			=	1;
		int producto				=	2;
		int listaCategorias			=	3;
		int	litsaInsumos			=	4;
		int listaInsumosxProducto	=	5;
		int	productosxCategoria		=	6;
		int insumosAdicionales		=	7;
	}
	
	private interface	Operaciones{
		int altaProducto		=	1;
		int actualizaProducto	=	2;
	}
	
	@Autowired
	ProductosDAO productosDAO;
	
	@Autowired
	CategoriasServicio categoriasServicio;
	
	@Autowired
	InsumosServicio insumosServicio;

	@Override
	public ProductosForm consulta(ProductosBean productosBean, int tipoConsulta) {
		ProductosForm productosForm = new ProductosForm();
		switch (tipoConsulta) {
		case Consultas.listaProductos:
			productosForm.setProductos(productosDAO.consultaProductos(productosBean));
			break;
		case Consultas.producto:
			productosForm.setProducto(productosDAO.consultaProducto(productosBean));
			break;
		case Consultas.listaCategorias:
			productosForm.setCategorias(categoriasServicio.lista(null, 1));
			break;
		case Consultas.litsaInsumos:
			productosForm.setInsumos(insumosServicio.consultaLista(null, 1));
			break;
		case Consultas.listaInsumosxProducto:
			productosForm.setInsumosxProductos(productosDAO.consultaInsumos(productosBean));
			break;
		case Consultas.productosxCategoria:
			productosForm.setProductos(productosDAO.consultaProductosxCategoria(productosBean));
			break;
		case Consultas.insumosAdicionales:
			productosForm.setInsumosxProductos(productosDAO.consultaInsumosAdicionales(productosBean));
			break;

		default:
			break;
		}
		
		return productosForm;
	}

	@Override
	public MensajeTransaccionBean grabaTransaccion(ProductosForm productosForm, int tipoOperacion) {
		MensajeTransaccionBean mensajeTransaccionBean = null;
		
		switch (tipoOperacion) {
		case Operaciones.altaProducto:
			mensajeTransaccionBean = altaProducto(productosForm);
			break;
		case Operaciones.actualizaProducto:
			mensajeTransaccionBean	=	actulizaProducto(productosForm);
			break;

		default:
			break;
		}
		
		return mensajeTransaccionBean;
	}
	
	@SuppressWarnings("unused")
	private MensajeTransaccionBean altaProducto(ProductosForm productosForm) {
		MensajeTransaccionBean mensajeTransaccionBean = null;
		MensajeTransaccionBean insumos = null;
		mensajeTransaccionBean = productosDAO.altaProducto(productosForm.getProducto());
		
		if(mensajeTransaccionBean.getNumeroMensaje() != 0) {
			return mensajeTransaccionBean;
		}
		
		int	numeroInsumos = productosForm.getInsumoAlta().length;
		
		for(int x = 0; x < numeroInsumos; x++) {
			InsumosxProductoBean insumosxProductoBean = new InsumosxProductoBean();
			insumosxProductoBean.setProductoId(mensajeTransaccionBean.getMensajeId());
			insumosxProductoBean.setInsumoId(productosForm.getInsumoAlta()[x]);
			insumosxProductoBean.setUnidades(productosForm.getUnidades()[x]);
			insumosxProductoBean.setSeleccionable(productosForm.getSeleccionable()[x]);
			
			insumos = productosDAO.altaInsumoxProducto(insumosxProductoBean);
		}
		
		return mensajeTransaccionBean;
	}
	
	private	MensajeTransaccionBean	actulizaProducto(ProductosForm productosForm) {
		MensajeTransaccionBean mensajeTransaccionBean = null;
		MensajeTransaccionBean insumos = null;
		
		mensajeTransaccionBean = productosDAO.actualizaProducto(productosForm.getProducto());
		
		if(mensajeTransaccionBean.getNumeroMensaje() != 0) {
			return mensajeTransaccionBean;
		}
		
		insumos = productosDAO.bajaInsumosxProducto(productosForm.getProducto());
		
		if(insumos.getNumeroMensaje() != 0) {
			return mensajeTransaccionBean;
		}
		
		int	numeroInsumos = productosForm.getInsumoAlta().length;
		
		for(int x = 0; x < numeroInsumos; x++) {
			InsumosxProductoBean insumosxProductoBean = new InsumosxProductoBean();
			insumosxProductoBean.setProductoId(productosForm.getProducto().getProductoId());
			insumosxProductoBean.setInsumoId(productosForm.getInsumoAlta()[x]);
			insumosxProductoBean.setUnidades(productosForm.getUnidades()[x]);
			insumosxProductoBean.setSeleccionable(productosForm.getSeleccionable()[x]);
			
			insumos = productosDAO.altaInsumoxProducto(insumosxProductoBean);
		}
		
		return mensajeTransaccionBean;
	}

}
