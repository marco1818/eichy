package com.eichy.spring.productos.servicios;

import com.eichy.spring.productos.beans.ProductosBean;
import com.eichy.spring.productos.beans.ProductosForm;
import com.eichy.spring.sistema.beans.MensajeTransaccionBean;

public interface ProductosServicio {
	public	ProductosForm consulta(ProductosBean productosBean, int tipoConsulta);
	public	MensajeTransaccionBean	grabaTransaccion(ProductosForm productosForm, int tipoOperacion);
}
