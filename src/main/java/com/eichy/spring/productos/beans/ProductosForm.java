package com.eichy.spring.productos.beans;
import java.math.BigDecimal;
import java.util.List;

import com.eichy.spring.categorias.beans.CategoriasBean;
import com.eichy.spring.insumos.InsumosBean;

public class ProductosForm {
	private ProductosBean producto;
	private List<ProductosBean> productos;
	private List<CategoriasBean> categorias;
	private List<InsumosBean> insumos;
	private List<InsumosxProductoBean> insumosxProductos;
	private int[] insumoAlta;
	private BigDecimal[] unidades;
	private int[] seleccionable;
	
	public ProductosBean getProducto() {
		return producto;
	}
	
	public void setProducto(ProductosBean producto) {
		this.producto = producto;
	}
	
	public List<ProductosBean> getProductos() {
		return productos;
	}
	
	public void setProductos(List<ProductosBean> productos) {
		this.productos = productos;
	}
	
	public List<CategoriasBean> getCategorias() {
		return categorias;
	}
	
	public void setCategorias(List<CategoriasBean> categorias) {
		this.categorias = categorias;
	}
	
	public List<InsumosBean> getInsumos() {
		return insumos;
	}
	
	public void setInsumos(List<InsumosBean> insumos) {
		this.insumos = insumos;
	}

	public int[] getInsumoAlta() {
		return insumoAlta;
	}

	public void setInsumoAlta(int[] insumoAlta) {
		this.insumoAlta = insumoAlta;
	}

	public BigDecimal[] getUnidades() {
		return unidades;
	}

	public void setUnidades(BigDecimal[] unidades) {
		this.unidades = unidades;
	}

	public int[] getSeleccionable() {
		return seleccionable;
	}

	public void setSeleccionable(int[] seleccionable) {
		this.seleccionable = seleccionable;
	}

	public List<InsumosxProductoBean> getInsumosxProductos() {
		return insumosxProductos;
	}

	public void setInsumosxProductos(List<InsumosxProductoBean> insumosxProductos) {
		this.insumosxProductos = insumosxProductos;
	}

	
}
