package com.eichy.spring.productos.beans;

import java.math.BigDecimal;

public class ProductosBean {
	private int	productoId;
	private int categoriaId;
	private String nombre;
	private BigDecimal precioUnitario;
	private BigDecimal precioMayoreo;
	private int unidadesMayoreo;
	private BigDecimal precioCompra;
	private int estatus;
	
	public ProductosBean() {
		
	}

	public int getProductoId() {
		return productoId;
	}

	public void setProductoId(int productoId) {
		this.productoId = productoId;
	}
	
	public void setProductoId(BigDecimal productoId) {
		this.productoId = productoId.intValue();
	}

	public int getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(int categoriaId) {
		this.categoriaId = categoriaId;
	}

	public void setCategoriaId(BigDecimal categoriaId) {
		this.categoriaId = categoriaId.intValue();
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public BigDecimal getPrecioUnitario() {
		return precioUnitario;
	}

	public void setPrecioUnitario(BigDecimal precioUnitario) {
		this.precioUnitario = precioUnitario;
	}

	public BigDecimal getPrecioMayoreo() {
		return precioMayoreo;
	}

	public void setPrecioMayoreo(BigDecimal precioMayoreo) {
		this.precioMayoreo = precioMayoreo;
	}

	public int getUnidadesMayoreo() {
		return unidadesMayoreo;
	}

	public void setUnidadesMayoreo(int unidadesMayoreo) {
		this.unidadesMayoreo = unidadesMayoreo;
	}
	
	public void setUnidadesMayoreo(BigDecimal unidadesMayoreo) {
		this.unidadesMayoreo = unidadesMayoreo.intValue();
	}

	public BigDecimal getPrecioCompra() {
		return precioCompra;
	}

	public void setPrecioCompra(BigDecimal precioCompra) {
		this.precioCompra = precioCompra;
	}

	public int getEstatus() {
		return estatus;
	}

	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}
	
	public void setEstatus(BigDecimal estatus) {
		this.estatus = estatus.intValue();
	}
	
}
