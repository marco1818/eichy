package com.eichy.spring.productos.beans;

import java.math.BigDecimal;

public class InsumosxProductoBean {
	private	int	productoId;
	private	int	insumoId;
	private	String descripcion;
	private BigDecimal unidades;
	private int seleccionable;
	private BigDecimal precioVenta;
	private	BigDecimal precioCompra;
	
	public InsumosxProductoBean() {
		
	}

	public int getProductoId() {
		return productoId;
	}

	public void setProductoId(int productoId) {
		this.productoId = productoId;
	}

	public void setProductoId(BigDecimal productoId) {
		this.productoId = productoId.intValue();
	}
	
	public int getInsumoId() {
		return insumoId;
	}

	public void setInsumoId(int insumoId) {
		this.insumoId = insumoId;
	}

	public void setInsumoId(BigDecimal insumoId) {
		this.insumoId = insumoId.intValue();
	}
	
	public BigDecimal getUnidades() {
		return unidades;
	}

	public void setUnidades(BigDecimal unidades) {
		this.unidades = unidades;
	}
	
	public int getSeleccionable() {
		return seleccionable;
	}

	public void setSeleccionable(int seleccionable) {
		this.seleccionable = seleccionable;
	}
	
	public void setSeleccionable(BigDecimal seleccionable) {
		this.seleccionable = seleccionable.intValue();
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public BigDecimal getPrecioVenta() {
		return precioVenta;
	}

	public void setPrecioVenta(BigDecimal precioVenta) {
		this.precioVenta = precioVenta;
	}

	public BigDecimal getPrecioCompra() {
		return precioCompra;
	}

	public void setPrecioCompra(BigDecimal precioCompra) {
		this.precioCompra = precioCompra;
	}
	
}