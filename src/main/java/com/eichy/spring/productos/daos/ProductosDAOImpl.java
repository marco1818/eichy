package com.eichy.spring.productos.daos;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.eichy.spring.productos.beans.InsumosxProductoBean;
import com.eichy.spring.productos.beans.ProductosBean;
import com.eichy.spring.sistema.beans.MensajeTransaccionBean;
import com.eichy.spring.sistema.daos.BaseDAO;

@Repository
public class ProductosDAOImpl extends BaseDAO implements ProductosDAO{

	private interface Funciones{
		String	consultaProductos			=	"PRODUCTOS_CONSULTA_PRODUCTOS";
		String	altaProductos				=	"PRODUCTOS_ALTA_PRODUCTOS";
		String	actualizaProductos			=	"PRODUCTOS_ACTUALIZA_PRODUCTOS";
		String	altaInsumosxProducto		=	"PRODUCTOS_ALTA_INSUMOS_X_PRODUCTO";
		String	actualizaInsumosxProducto	=	"PRODUCTOS_ACTUALIZA_INSUMOS_X_PRODUCTO";
		String	consultaInsumosxProducto	=	"PRODUCTOS_CONSULTA_INSUMOS_X_PRODUCTO";
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ProductosBean> consultaProductos(ProductosBean productosBean) {
		List<ProductosBean> productos	= null;
		ArrayList<Object> parametros	= new ArrayList<>();
		parametros.add(1);
		parametros.add(0);
		parametros.add(0);
		
		productos = (List<ProductosBean>) ejecutaFuncionAll(Funciones.consultaProductos, parametros, ProductosBean.class);
		
		return productos;
	}

	@Override
	public ProductosBean consultaProducto(ProductosBean productosBean) {
		ProductosBean producto	= null;
		ArrayList<Object> parametros	= new ArrayList<>();
		parametros.add(2);
		parametros.add(productosBean.getProductoId());
		parametros.add(0);
		producto = (ProductosBean) ejecutaFuncion(Funciones.consultaProductos, parametros, ProductosBean.class);
		
		return producto;
	}

	@Override
	public MensajeTransaccionBean altaProducto(ProductosBean productosBean) {
		MensajeTransaccionBean mensajeTransaccionBean = null;
		ArrayList<Object> parametros = new ArrayList<>();
		parametros.add(productosBean.getCategoriaId());
		parametros.add(productosBean.getNombre());
		parametros.add(productosBean.getPrecioUnitario());
		parametros.add(productosBean.getPrecioMayoreo());
		parametros.add(productosBean.getUnidadesMayoreo());
		parametros.add(productosBean.getPrecioCompra());
		parametros.add(productosBean.getEstatus());
		parametros.add(1);
		parametros.add(2);
		
		mensajeTransaccionBean = (MensajeTransaccionBean) ejecutaFuncion(Funciones.altaProductos, parametros, MensajeTransaccionBean.class);
		
		return mensajeTransaccionBean;
	}

	@Override
	public MensajeTransaccionBean actualizaProducto(ProductosBean productosBean) {
		MensajeTransaccionBean mensajeTransaccionBean = null;
		ArrayList<Object> parametros = new ArrayList<>();
		parametros.add(productosBean.getProductoId());
		parametros.add(productosBean.getCategoriaId());
		parametros.add(productosBean.getNombre());
		parametros.add(productosBean.getPrecioUnitario());
		parametros.add(productosBean.getPrecioMayoreo());
		parametros.add(productosBean.getUnidadesMayoreo());
		parametros.add(productosBean.getPrecioCompra());
		parametros.add(productosBean.getEstatus());
		parametros.add(1);
		parametros.add(2);
		
		mensajeTransaccionBean = (MensajeTransaccionBean) ejecutaFuncion(Funciones.actualizaProductos, parametros, MensajeTransaccionBean.class);
		
		return mensajeTransaccionBean;
	}

	@Override
	public MensajeTransaccionBean altaInsumoxProducto(InsumosxProductoBean insumosxProductoBean) {
		MensajeTransaccionBean mensajeTransaccionBean = null;
		ArrayList<Object> parametros = new ArrayList<>();
		parametros.add(insumosxProductoBean.getProductoId());
		parametros.add(insumosxProductoBean.getInsumoId());
		parametros.add(insumosxProductoBean.getUnidades());
		parametros.add(insumosxProductoBean.getSeleccionable());
		parametros.add(1);
		parametros.add(2);
		
		mensajeTransaccionBean = (MensajeTransaccionBean) ejecutaFuncion(Funciones.altaInsumosxProducto, parametros, MensajeTransaccionBean.class);
		
		return mensajeTransaccionBean;
	}

	@Override
	public MensajeTransaccionBean bajaInsumosxProducto(ProductosBean productosBean) {
		MensajeTransaccionBean mensajeTransaccionBean = null;
		ArrayList<Object> parametros = new ArrayList<>();
		parametros.add(1);
		parametros.add(productosBean.getProductoId());
		parametros.add(0);
		parametros.add(0);
		parametros.add(0);
		parametros.add(1);
		parametros.add(2);

		mensajeTransaccionBean = (MensajeTransaccionBean) ejecutaFuncion(Funciones.actualizaInsumosxProducto, parametros, MensajeTransaccionBean.class);
		
		return mensajeTransaccionBean;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<InsumosxProductoBean> consultaInsumos(ProductosBean productosBean) {
		List<InsumosxProductoBean> insumos = null;
		ArrayList<Object> parametros = new ArrayList<>();
		parametros.add(1);
		parametros.add(productosBean.getProductoId());
		
		insumos = (List<InsumosxProductoBean>) ejecutaFuncionAll(Funciones.consultaInsumosxProducto, parametros, InsumosxProductoBean.class);
		
		return insumos;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProductosBean> consultaProductosxCategoria(ProductosBean productosBean) {
		List<ProductosBean> productos	= null;
		ArrayList<Object> parametros	= new ArrayList<>();
		parametros.add(3);
		parametros.add(0);
		parametros.add(productosBean.getCategoriaId());
		
		productos = (List<ProductosBean>) ejecutaFuncionAll(Funciones.consultaProductos, parametros, ProductosBean.class);
		
		return productos;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<InsumosxProductoBean> consultaInsumosAdicionales(ProductosBean productosBean) {
		List<InsumosxProductoBean> insumos = null;
		ArrayList<Object> parametros = new ArrayList<>();
		parametros.add(2);
		parametros.add(productosBean.getProductoId());
		
		insumos = (List<InsumosxProductoBean>) ejecutaFuncionAll(Funciones.consultaInsumosxProducto, parametros, InsumosxProductoBean.class);
		
		return insumos;
	}

}
