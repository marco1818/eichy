package com.eichy.spring.productos.daos;

import java.util.List;

import com.eichy.spring.productos.beans.InsumosxProductoBean;
import com.eichy.spring.productos.beans.ProductosBean;
import com.eichy.spring.sistema.beans.MensajeTransaccionBean;

public interface ProductosDAO {
	public	List<ProductosBean> consultaProductos(ProductosBean productosBean);
	public	List<ProductosBean> consultaProductosxCategoria(ProductosBean productosBean);
	public	ProductosBean consultaProducto(ProductosBean productosBean);
	public	List<InsumosxProductoBean> consultaInsumos(ProductosBean productosBean);
	public	List<InsumosxProductoBean> consultaInsumosAdicionales(ProductosBean productosBean);
	public	MensajeTransaccionBean	altaProducto(ProductosBean productosBean);
	public	MensajeTransaccionBean	actualizaProducto(ProductosBean productosBean);
	public	MensajeTransaccionBean	altaInsumoxProducto(InsumosxProductoBean insumosxProductoBean);
	public	MensajeTransaccionBean	bajaInsumosxProducto(ProductosBean productosBean);
}
