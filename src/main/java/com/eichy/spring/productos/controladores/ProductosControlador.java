package com.eichy.spring.productos.controladores;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.eichy.spring.productos.beans.ProductosForm;
import com.eichy.spring.productos.servicios.ProductosServicio;
import com.eichy.spring.sistema.beans.MensajeTransaccionBean;

@Controller
public class ProductosControlador {

	@InitBinder
	public void InitBinder(WebDataBinder webDataBinder, WebRequest webRequest) {
		webDataBinder.setDisallowedFields("");
	}	
	
	@Autowired
	ProductosServicio productosServicio;
	
	@RequestMapping(value= {"/productos.htm"}, method= RequestMethod.GET)
	public	ModelAndView inicio() {
		ModelAndView modelAndView = new ModelAndView();
		ProductosForm productosForm = new ProductosForm();
		
		productosForm.setProductos(productosServicio.consulta(null, 1).getProductos());
		productosForm.setCategorias(productosServicio.consulta(null, 3).getCategorias());
		productosForm.setInsumos(productosServicio.consulta(null, 4).getInsumos());
		
		modelAndView.setViewName("productos/productos");
		modelAndView.addObject("productosForm",productosForm);
		
		return modelAndView;
	}
	
	@RequestMapping(value= {"/productos.htm"}, method= RequestMethod.POST)
	public	ModelAndView transacciones(@ModelAttribute("productosForm") ProductosForm productosForm, @RequestParam int tipoOperacion) {
		ModelAndView modelAndView = new ModelAndView();
		
		MensajeTransaccionBean mensajeTransaccionBean = null;
		mensajeTransaccionBean = productosServicio.grabaTransaccion(productosForm, tipoOperacion);
		
		modelAndView.setViewName("sistema/resultadoTransaccion");
		modelAndView.addObject("mensajeTransaccionBean",mensajeTransaccionBean);
		return modelAndView;
	}
}
