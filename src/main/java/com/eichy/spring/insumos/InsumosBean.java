package com.eichy.spring.insumos;

import java.math.BigDecimal;

import com.eichy.spring.sistema.beans.BaseBean;

public class InsumosBean extends BaseBean{
	private	int	insumoId;
	private String descripcion;
	private int existencias;
	private int tipoUnidadId;
	private	BigDecimal	precioCompra;
	private BigDecimal	precioVenta;
	private int estatus;
	
	public InsumosBean() {
		
	}

	public int getInsumoId() {
		return insumoId;
	}

	public void setInsumoId(int insumoId) {
		this.insumoId = insumoId;
	}

	public void setInsumoId(BigDecimal insumoId) {
		this.insumoId = insumoId.intValue();
	}
	
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getExistencias() {
		return existencias;
	}

	public void setExistencias(int existencias) {
		this.existencias = existencias;
	}
	
	public void setExistencias(BigDecimal existencias) {
		this.existencias = existencias.intValue();
	}

	public int getTipoUnidadId() {
		return tipoUnidadId;
	}

	public void setTipoUnidadId(int tipoUnidadId) {
		this.tipoUnidadId = tipoUnidadId;
	}

	public void setTipoUnidadId(BigDecimal tipoUnidadId) {
		this.tipoUnidadId = tipoUnidadId.intValue();
	}

	public int getEstatus() {
		return estatus;
	}

	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}
	
	public void setEstatus(BigDecimal estatus) {
		this.estatus = estatus.intValue();
	}

	public BigDecimal getPrecioCompra() {
		return precioCompra;
	}

	public void setPrecioCompra(BigDecimal precioCompra) {
		this.precioCompra = precioCompra;
	}

	public BigDecimal getPrecioVenta() {
		return precioVenta;
	}

	public void setPrecioVenta(BigDecimal precioVenta) {
		this.precioVenta = precioVenta;
	}
	
}
