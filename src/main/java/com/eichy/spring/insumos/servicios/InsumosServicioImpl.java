package com.eichy.spring.insumos.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eichy.spring.insumos.InsumosBean;
import com.eichy.spring.insumos.daos.InsumosDAO;
import com.eichy.spring.sistema.beans.MensajeTransaccionBean;
import com.eichy.spring.sistema.servicios.BaseServicio;

@Service
public class InsumosServicioImpl extends BaseServicio implements InsumosServicio{

	private	interface Operaciones{
		int	altaInsumo		=	1;
		int actualizaInsumo	=	2;
	}

	@Autowired
	InsumosDAO insumosDAO;
	
	@Override
	public InsumosBean consulta(InsumosBean insumosBean, int tipoConsulta) {
		switch (tipoConsulta) {
		case 1:
			insumosBean = insumosDAO.consultaInsumo(insumosBean);
			break;

		default:
			break;
		}
		
		return insumosBean;
	}

	@Override
	public MensajeTransaccionBean grabaTransaccion(InsumosBean insumosBean, int tipoOperacion) {
		MensajeTransaccionBean mensajeTransaccionBean = null;
		insumosBean.setNumeroTransaccion(numeroTransaccion());
		
		switch (tipoOperacion) {
		case Operaciones.altaInsumo:
			mensajeTransaccionBean = insumosDAO.altaInsumo(insumosBean);
			break;
		case Operaciones.actualizaInsumo:
			mensajeTransaccionBean = insumosDAO.actualizaInsumo(insumosBean);
			break;

		default:
			break;
		}
		
		return mensajeTransaccionBean;
	}

	@Override
	public List<InsumosBean> consultaLista(InsumosBean insumosBean, int tipoConsulta) {
		List<InsumosBean> insumos = null;
		switch (tipoConsulta) {
		case 1:
			insumos = insumosDAO.insumosActivos(insumosBean);
			break;
			
		default:
			break;
		}
		return insumos;
	}

}
