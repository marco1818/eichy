package com.eichy.spring.insumos.servicios;

import java.util.List;

import com.eichy.spring.insumos.InsumosBean;
import com.eichy.spring.sistema.beans.MensajeTransaccionBean;

public interface InsumosServicio {
	public	InsumosBean consulta(InsumosBean insumosBean, int tipoConsulta);
	public	List<InsumosBean> consultaLista(InsumosBean insumosBean, int tipoConsulta);
	public	MensajeTransaccionBean grabaTransaccion(InsumosBean insumosBean, int tipoOperacion);
}
