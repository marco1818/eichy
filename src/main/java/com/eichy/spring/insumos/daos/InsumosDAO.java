package com.eichy.spring.insumos.daos;

import java.util.List;

import com.eichy.spring.insumos.InsumosBean;
import com.eichy.spring.sistema.beans.MensajeTransaccionBean;

public interface InsumosDAO {
	public	InsumosBean consultaInsumo(InsumosBean insumosBean);
	public	List<InsumosBean>  insumosActivos(InsumosBean insumosBean);
	public	MensajeTransaccionBean altaInsumo(InsumosBean insumosBean);
	public	MensajeTransaccionBean actualizaInsumo(InsumosBean insumosBean);
}
