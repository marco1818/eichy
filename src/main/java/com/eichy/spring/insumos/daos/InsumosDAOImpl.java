package com.eichy.spring.insumos.daos;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.eichy.spring.insumos.InsumosBean;
import com.eichy.spring.sistema.beans.MensajeTransaccionBean;
import com.eichy.spring.sistema.daos.BaseDAO;

@Repository
public class InsumosDAOImpl extends BaseDAO implements InsumosDAO{
	
	private	interface	Funciones{
		String	altaInsumo				=	"INSUMOS_ALTA_INSUMOS";
		String	actualizaInsumo			=	"INSUMOS_ACTUALIZA_INSUMO";
		String	consultaInsumosActivos	=	"INSUMOS_CONSULTA_INSUMOS_ACTIVOS";
		String	consultaInsumos			=	"INSUMOS_CONSULTA_INSUMOS";
	}

	@Override
	public MensajeTransaccionBean altaInsumo(InsumosBean insumosBean) {
		MensajeTransaccionBean mensajeTransaccionBean = null;
		ArrayList<Object> parametros = new ArrayList<>();
		parametros.add(insumosBean.getDescripcion());
		parametros.add(insumosBean.getTipoUnidadId());
		parametros.add(insumosBean.getPrecioCompra());
		parametros.add(insumosBean.getPrecioVenta());
		parametros.add(insumosBean.getEstatus());
		parametros.add(insumosBean.getUsuarioId());
		parametros.add(insumosBean.getNumeroTransaccion());
		
		mensajeTransaccionBean = (MensajeTransaccionBean) ejecutaFuncion(Funciones.altaInsumo, parametros, MensajeTransaccionBean.class);
		
		return mensajeTransaccionBean;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<InsumosBean> insumosActivos(InsumosBean insumosBean) {
		List<InsumosBean> insumos = null;
		ArrayList<Object> parametros = new ArrayList<>();
		parametros.add(1);
		parametros.add(0);
		
		insumos = (List<InsumosBean>) ejecutaFuncionAll(Funciones.consultaInsumosActivos, parametros, InsumosBean.class);
		
		return insumos;
	}

	@Override
	public InsumosBean consultaInsumo(InsumosBean insumosBean) {
		InsumosBean insumos = null;
		ArrayList<Object> parametros = new ArrayList<>();
		parametros.add(2);
		parametros.add(insumosBean.getInsumoId());
		
		insumos =  (InsumosBean) ejecutaFuncion(Funciones.consultaInsumos, parametros, InsumosBean.class);
		
		return insumos;
	}

	@Override
	public MensajeTransaccionBean actualizaInsumo(InsumosBean insumosBean) {
		MensajeTransaccionBean mensajeTransaccionBean = null;
		ArrayList<Object> parametros = new ArrayList<>();
		parametros.add(1);
		parametros.add(insumosBean.getInsumoId());
		parametros.add(insumosBean.getDescripcion());
		parametros.add(insumosBean.getExistencias());
		parametros.add(insumosBean.getTipoUnidadId());
		parametros.add(insumosBean.getPrecioCompra());
		parametros.add(insumosBean.getPrecioVenta());
		parametros.add(insumosBean.getEstatus());
		parametros.add(insumosBean.getUsuarioId());
		parametros.add(insumosBean.getNumeroTransaccion());
		
		mensajeTransaccionBean = (MensajeTransaccionBean) ejecutaFuncion(Funciones.actualizaInsumo, parametros, MensajeTransaccionBean.class);
		
		return mensajeTransaccionBean;
	}

}
