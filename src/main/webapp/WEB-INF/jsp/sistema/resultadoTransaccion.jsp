<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/sistema/sistema.css">
<script type="text/javascript">
	$('#aceptarMsg').click(function(){
		$('#contenido').unblock();
	});
</script>
</head>
<body>
	<div class="mensaje" style="width:90%; background-color: white; margin: 0 auto;">
		<table style=" width: 100%;margin: 0 auto; border-collapse: collapse;" >
			<tr class="encabezadoTabla">
				<td colspan="2" style="border-radius: 5px 5px 5px 5px;">
					Resultado de la Operacion.
				</td>
			</tr>
			<tr >
				<td colspan="2">&nbsp;
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label>${mensajeTransaccionBean.mensaje}</label>
				</td>
			</tr>
			<tr >
				<td colspan="2">&nbsp;
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div class="navResultado resultado-bordeado">
						<ul class="opciones-navbar">
							<li id="consultaVacantesSAP"><a id="aceptarMsg" href="javascript:void(0);">Aceptar</a></li>
						</ul>
					</div>
				</td>
			
			</tr>
			<tr>
				<td colspan="2">
					&nbsp;
				</td>
			</tr>
		</table>
	</div>
	
	<form id="formaOculta">
		<input type="hidden" id="numeroMensaje" name="numeroMensaje" value="${mensajeTransaccionBean.numeroMensaje}">
	</form>
	
</body>

</html>

