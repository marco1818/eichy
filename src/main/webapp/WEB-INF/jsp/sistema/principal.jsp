<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Eichy</title>

<script src='<%=request.getContextPath()%>/js/Jquery/jquery-1.9.1.js'></script>
<script src='<%=request.getContextPath()%>/js/Jquery/jquery.blockUI.js'></script>
<script type='text/javascript' src='/eichy/dwr/util.js'></script>
<script type='text/javascript' src='/eichy/dwr/engine.js'></script>
<script type='text/javascript' src='/eichy/dwr/interface/InsumosServicioImpl.js'></script>
<script src='<%=request.getContextPath()%>/js/insumos/insumos.js'></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/utilerias/utilerias.js"></script>
</head>
<style>
body{
	background-color: #FAFAFA;
	font-family: Helvetica;
}

.contenido{
	font-family: Helvetica;
	font-size: 10px;
	margin:0px; 
	padding:0px; 
	border:0px; 
	height: 100%;
}
	.border{
		border: 1px solid; 
		border-radius: 5px 5px 5px 5px; 
		border-color: #ccd5de;
		padding: 1%;
	}
	
	select{
	font-size: 11px;
}

legend{
	color: #2f5475;
	font-family: 'Helvetica';
	font-weight: bold;
	font-size: 13px;
	padding: 10px;
}

fieldset{
border-radius: 5px 5px 5px 5px; 
	border-color: #ccd5de;
}
</style>
<body>
<form:form id="insumosBean" action="insumos1.htm" name="insumosBean" modelAttribute="insumosBean">
	<input type="hidden" id="tipoOperacion" name="tipoOperacion" value="1">
	<div style="width: 100%;" >
		<div id="header" style="width: 100%; height: 100px; " class="border">
			
		</div>		
		<br>
		<div id="menu" style="width: 20%; float:left;"  class="border">
			
		</div>
		<div id="contenido" style="width: 75%; float: right;" class="contenido">
			<div style="width: 100%;">
				<fieldset>
					<legend>INSUMOS</legend>
					
					<table width="100%" cellspacing="3" cellpadding="5">
						<tr>
							
						</tr>
						<tr>
							<td width="10%" align="right">
								<label>Insumo:</label>
							</td>
							<td width="20%" align="left">
								<select	id="insumoId" name="insumoId">
									<option value="0">NUEVO INSUMO</option>
									<c:forEach items="${insumosLista}" var="insumo">
										<option value="${insumo.insumoId}">${insumo.descripcion}</option>
									</c:forEach>
								</select>
							</td>
							<td width="10%">
							</td>
							<td width="10%;">
							</td>
							
							<td width="20%;">
							</td>
							<td width="30%;">
							</td>
						</tr>
						<tr>
							<td align="right">
								<label>Nombre: </label>
							</td>
							<td align="left">
								<input id="descripcion" name="descripcion">
							</td>
							<td></td>
							<td align="right">
								<label>Precio Venta: </label>
							</td>
							<td align="left">
								<input id="precioVenta" name="precioVenta">
							</td>
							<td></td>
						</tr>
						<tr>
							<td align="right">
								<label>Unidad: </label>
							</td>
							<td align="left">
								<select id="tipoUnidadId" name="tipoUnidadId">
									<option value="1">PIEZA</option>
									<option value="2">LITROS</option>
									<option value="3">GRAMOS</option>
									<option value="4">KILOS</option>
								</select>
							</td>
							<td></td>
							
							<td align="right">
								<label>Precio Compra:</label>
							</td>
							<td align="left">
								<input id="precioCompra" name="precioCompra">
							</td>
							
							<td></td>
						</tr>
						<tr style="display: none;">
							<td align="right">
								<label>Activo: </label>
							</td>
							<td align="left">
								<input type="text" id="estatus" name="estatus" value="1">
							</td>
						</tr>
						<tr>
							<td colspan="4"></td>
							<td align="right">
								<button type="button" id="btnAceptar">Aceptar</button>
							</td>
							<td></td>
						</tr>
					</table>	
				</fieldset>
			
			</div>
			<br><br><br>
		</div>
		
	</div>
	<div id="mensaje" style="display: none;"/></div>
</form:form>

</body>
</html>