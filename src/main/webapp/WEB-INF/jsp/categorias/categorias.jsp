<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Eichy</title>

<script src='<%=request.getContextPath()%>/js/Jquery/jquery-1.9.1.js'></script>
<script src='<%=request.getContextPath()%>/js/Jquery/jquery.blockUI.js'></script>
<script type='text/javascript' src='/eichy/dwr/util.js'></script>
<script type='text/javascript' src='/eichy/dwr/engine.js'></script>
<script type='text/javascript' src='/eichy/dwr/interface/CategoriasServicioImpl.js'></script>
<script src='<%=request.getContextPath()%>/js/categorias/categorias.js'></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/utilerias/utilerias.js"></script>
</head>
<style>
body{
	background-color: #FAFAFA;
	font-family: Helvetica;
}

.contenido{
	font-family: Helvetica;
	font-size: 10px;
	margin:0px; 
	padding:0px; 
	border:0px; 
	height: 100%;
}
	.border{
		border: 1px solid; 
		border-radius: 5px 5px 5px 5px; 
		border-color: #ccd5de;
		padding: 1%;
	}
	
	select{
	font-size: 11px;
}

legend{
	color: #2f5475;
	font-family: 'Helvetica';
	font-weight: bold;
	font-size: 13px;
	padding: 10px;
}

fieldset{
border-radius: 5px 5px 5px 5px; 
	border-color: #ccd5de;
}
</style>
<body>
<form:form id="categoriasBean" action="categoriasTranccion.htm" name="categoriasBean" modelAttribute="categoriasBean">
	<input type="hidden" id="tipoOperacion" name="tipoOperacion" value="1">
	<div style="width: 100%;" >
		<div id="header" style="width: 100%; height: 100px; " class="border">
			
		</div>		
		<br>
		<div id="menu" style="width: 20%; float:left;"  class="border">
			
		</div>
		<div id="contenido" style="width: 75%; float: right;" class="contenido">
			<div style="width: 100%;">
				<fieldset>
					<legend>CATEGORIAS</legend>
					
					<table width="100%" cellspacing="3" cellpadding="5">
						<tr>
							
						</tr>
						<tr>
							<td width="10%" align="right">
								<label>Categoria:</label>
							</td>
							<td width="20%" align="left">
								<select	id="categoriaId" name="categoriaId">
									<option value="0">NUEVA CATEGORIA</option>
									<c:forEach items="${categorias}" var="categorias">
										<option value="${categorias.categoriaId}">${categorias.nombre}</option>
									</c:forEach>
								</select>
							</td>

						</tr>
						<tr>
							<td align="right">
								<label>Nombre: </label>
							</td>
							<td align="left">
								<input id="nombre" name="nombre">
							</td>

						</tr>
						<tr>
							<td colspan="2"></td>
							<td align="right">
								<button type="button" id="btnAceptar">Aceptar</button>
							</td>
							<td></td>
						</tr>
					</table>	
				</fieldset>
			
			</div>
			<br><br><br>
		</div>
		
	</div>
	<div id="mensaje" style="display: none;"/></div>
</form:form>

</body>
</html>