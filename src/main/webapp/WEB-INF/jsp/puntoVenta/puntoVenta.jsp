<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>PuntoVenta</title>
<script src='<%=request.getContextPath()%>/js/Jquery/jquery-1.9.1.js'></script>
<script src='<%=request.getContextPath()%>/js/Jquery/jquery.blockUI.js'></script>
<script type='text/javascript' src='/eichy/dwr/util.js'></script>
<script type='text/javascript' src='/eichy/dwr/engine.js'></script>
<script type='text/javascript' src='/eichy/dwr/interface/CategoriasServicioImpl.js'></script>
<script type='text/javascript' src='/eichy/dwr/interface/ProductosServicioImpl.js'></script>
<script type='text/javascript' src='/eichy/dwr/interface/InsumosServicioImpl.js'></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/puntoVenta/puntoVenta.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/utilerias/utilerias.js"></script>
</head>
<body>

<form:form id="puntoVentaForm" action="puntoVentaTransaccion.htm" name="puntoVentaForm" modelAttribute="puntoVentaForm">
	<input type="hidden" id="tipoOperacion" name="tipoOperacion" value="1">
	<div style="width: 100%;" >
		<div id="header" style="width: 100%; height: 100px; " class="border">
			
		</div>		
		<br>
		<div id="menu" style="width: 20%; float:left;"  class="border">
			
		</div>
		<div id="contenido" style="width: 79%; float: right;" class="contenido">
			<div id="puntoVenta" style="width: 60%; float: left;">
				<fieldset>
					<legend>PUNTO VENTA</legend>
					<div id="categorias" style="width: 100%;">
						<table width="100%">
							<%int contador=1;%>
							<c:forEach items="${puntoVentaForm.categorias}" var="categoria">
								<%
									if (contador == 1){
								%>
										<tr>
								<%
									}
								%>
									<td width="33%" height="150px;" style="text-align: center;">
										<label>${categoria.nombre }</label>
										<a>
											<img style="cursor: pointer;" width="100%" height="90%" src="imagenes/seguridad/congelados.jpg" onclick="consultaProductos(${categoria.categoriaId});">
										</a>
									<td>
									
								<%
									contador+=1;
									if (contador > 3){
										contador=1;
								%>
										</tr>
								<%
									}
								%>
									
							</c:forEach>
						</table>
					</div>
					<div id="productos" style="width: 100%; display: none;">
						<table style="width: 100%;">
							<tr>
								<td align="right" width="100%">
									<button>Regresar</button>
								</td>
							</tr>
						</table>
						<br>
						<table id="tablaProductos" width="100%" style="margin: 0 auto;">
							<tbody></tbody>
						</table>
					</div>
					<div id="insumos" style="width: 100%; display: none;">
					
					</div>
					<div id="productosTmp" style="">
						
					</div>
					<div id="adicionalesTmp">
					
					</div>
				</fieldset>
			</div>
			<div style="width: 38%; float: right;">
				<fieldset>
					<legend>COMANDA</legend>
					<div id="comanda" style="width: 100%;">
						<table id="tblComanda" width="100%">
							<thead>
								<tr>
									<th style="text-align: left;">
										<label>PRODUCTO</label>
									</th>
									<th style="text-align: right;">
										<label>PRECIO</label>
									</th>
								</tr>
								<tr></tr>
								<tr></tr>
							</thead>
							<tbody></tbody>
							<tfoot>
								<tr></tr>
								<tr></tr>
								<tr>
									<td style="text-align: left;">
										<label>TOTAL:</label>
									</td>
									<td style="text-align: right;">
										<label id="lblTotal">0.00</label>
									</td>
								</tr>
							</tfoot>
						</table>
					</div>
				</fieldset>
			</div>
		</div>
		
	</div>
	<div id="mensaje" style="display: none;"/></div>
</form:form>

</body>
</html>