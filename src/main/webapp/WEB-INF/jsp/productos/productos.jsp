<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Eichy</title>

<script src='<%=request.getContextPath()%>/js/Jquery/jquery-1.9.1.js'></script>
<script src='<%=request.getContextPath()%>/js/Jquery/jquery.blockUI.js'></script>
<script type='text/javascript' src='/eichy/dwr/util.js'></script>
<script type='text/javascript' src='/eichy/dwr/engine.js'></script>
<script type='text/javascript' src='/eichy/dwr/interface/ProductosServicioImpl.js'></script>
<script type='text/javascript' src='/eichy/dwr/interface/CategoriasServicioImpl.js'></script>
<script type='text/javascript' src='/eichy/dwr/interface/InsumosServicioImpl.js'></script>
<script src='<%=request.getContextPath()%>/js/productos/productos.js'></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/utilerias/utilerias.js"></script>
</head>
<style>
body{
	background-color: #FAFAFA;
	font-family: Helvetica;
}

.contenido{
	font-family: Helvetica;
	font-size: 10px;
	margin:0px; 
	padding:0px; 
	border:0px; 
	height: 100%;
}
	.border{
		border: 1px solid; 
		border-radius: 5px 5px 5px 5px; 
		border-color: #ccd5de;
		padding: 1%;
	}
	
	select{
	font-size: 11px;
}

legend{
	color: #2f5475;
	font-family: 'Helvetica';
	font-weight: bold;
	font-size: 13px;
	padding: 10px;
}

fieldset{
border-radius: 5px 5px 5px 5px; 
	border-color: #ccd5de;
}
</style>
<body>
<form:form id="productosForm" action="productos.htm" name="productosForm" modelAttribute="productosForm">
	<input type="hidden" id="tipoOperacion" name="tipoOperacion" value="1">
	<div style="width: 100%;" >
		<div id="header" style="width: 100%; height: 100px; " class="border">
			
		</div>		
		<br>
		<div id="menu" style="width: 20%; float:left;"  class="border">
			
		</div>
		<div id="contenido" style="width: 75%; float: right;" class="contenido">
			<div style="width: 100%;">
				<fieldset>
					<legend>PRODUCTOS</legend>
					<table width="100%">
						<tr>
							<td width="90%" align="right">
								<button type="button" id="btnAceptar">Aceptar</button>
							</td>
						</tr>
					</table>
					<br>
					<table width="100%">
						<tr>
							<td>
								<label>Producto: </label>
							</td>
							<td width="20%" align="left">
								<select	id="productoId" name="producto.productoId">
									<option value="0">NUEVO PRODUCTO</option>
									<c:forEach items="${productosForm.productos}" var="producto">
										<option value="${producto.productoId}">${producto.nombre}</option>
									</c:forEach>
								</select>
							</td>
						</tr>
						<tr>
							<td>
								<label>Nombre: </label>
							</td>
							<td>
								<input type="text" id="nombre" name="producto.nombre">
							</td>
							<td class="separador">
							
							</td>
							<td>
								<label>Categoria: </label>
							</td>
							<td>
								<select	id="categoriaId" name="producto.categoriaId">
									<option value="0">SELECCIONE UNA CATEGORIA</option>
									<c:forEach items="${productosForm.categorias}" var="categoria">
										<option value="${categoria.categoriaId}">${categoria.nombre}</option>
									</c:forEach>
								</select>
							</td>
						</tr>
						<tr>
							<td>
								<label>Precio Unitario: </label>
							</td>
							<td>
								<input type="text" id="precioUnitario" name="producto.precioUnitario" value="0">
							</td>
							<td class="separador">
							
							</td>
							<td>
								<label>Precio Mayoreo: </label>
							</td>
							<td>
								<input type="text" id="precioMayoreo" name="producto.precioMayoreo" value="0">
							</td>
						</tr>
						<tr>
							<td>
								<label>Unidades Mayoreo:</label>
							</td>
							<td>
								<input type="text" id="unidadesMayoreo" name="producto.unidadesMayoreo" value="0">
							</td>
							<td class="separador">
							
							</td>
							<td>
								<label>Precio Compra: </label>
							</td>
							<td>
								<input type="text" id="precioCompraProducto1" disabled="disabled" value="0">
								<input type="hidden" id="precioCompraProducto" name="producto.precioCompra" value="0">
							</td>
						</tr>
						<tr>
							<td>
								<label>Estatus: </label>
							</td>
							<td>
								<select id="estatus" name="producto.estatus">
									<option value="1">ACTIVO</option>
									<option value="0">INACTIVO</option>
								</select>
							</td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</table>
					<br>
					<br>
				</fieldset>
								
				<fieldset>
					<legend>INSUMOS</legend>
					<table width="100%" >
						<tr>
							<td align="right" colspan="5">
								<button type="button" id="btnAgregarInsumo">Agregar</button>
							</td>
						</tr>
						<tr>
							<td>
								<label>Insumo: </label>
							</td>
							<td>
								<select id="insumoId">
									<option value="0">SELECCIONE UN INSUMO</option>
									<c:forEach items="${productosForm.insumos}" var="insumos">
										<option value="${insumos.insumoId}">${insumos.descripcion}</option>
									</c:forEach>
								</select>
							</td>
							<td></td>
							<td>
								<label>Precio Venta: </label>
							</td>
							<td>
								<input type="text" id="precioVentaInsumo" disabled="disabled">
							</td>
						</tr>
						<tr>
							<td>
								<label>Precio Compra:</label>
							</td>
							<td>
								<input type="text" id="precioCompraInsumo" disabled="disabled">
							</td>
							<td>
							</td>
							<td>
								<label>Unidades: </label>
							</td>
							<td>
								<input type="text" id="unidades">
							</td>
						</tr>
						<tr>
							<td>
								<label>Seleccionable: </label>
							</td>
							<td>
								<input type="checkbox" id="seleccionable" >
							</td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						
					</table>
					<br>
					<br>
					<div id="insummos" style="width: 100%; margin: 0 auto;">
						<table id="tablaInsumos" width="50%" style="margin: 0 auto;" border="1">
							<thead>
								<tr>
									<th>
										<label>INSUMO</label>
									</th>
									<th>
										<label>PRECIO VENTA</label>
									</th>
									<th>
										<label>PRECIO COMPRA</label>
									</th>
									<th>
										<label>UNIDADES</label>
									</th>
									<th>
										<label>SELECCIONABLE</label>
									</th>
									<th>
										<label>ACCION</label>
									</th>
								</tr>
							</thead>
							<tbody>
							
							</tbody>
							
						</table>
						<br>
						<br>
					</div>
				</fieldset>
			</div>
			<br><br><br>
		</div>
		
	</div>
	<div id="mensaje" style="display: none;"/></div>
</form:form>

</body>
</html>