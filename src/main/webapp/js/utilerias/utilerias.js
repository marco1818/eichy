function bloqueaPantalla(divBloquear, divMensaje){
	
	var jqBloquear	=	eval("'#"+divBloquear+"'");
	var jqMensaje	=	eval("'#"+divMensaje+"'");
	$(jqMensaje).html('<img src="imagenes/seguridad/barras.jpg" alt=""/>');
	
	$(jqBloquear).block({
		message: $(jqMensaje),
		css: {
			border:		'none',
			background:	'none'
			}
	});
	
}

function desbloqueaPantalla(divDesBloquearBloquear){
	var jqDesBloquear	=	eval("'#"+divDesBloquearBloquear+"'");
	$(jqDesBloquear).unblock();
}


function grabaTransaccionRecarga(formId, divContenedor, divMensaje, funcionExito, funcionError){
	var jqFormaId	=	eval("'#"+formId+"'");
	var url			=	$(jqFormaId).attr('action');
	var jqBloquear	=	eval("'#"+divContenedor+"'");
	var jqMensaje	=	eval("'#"+divMensaje+"'");
	$(jqMensaje).html('<img src="imagenes/seguridad/barras.jpg" alt=""/>');
	
	$(jqBloquear).block({
		message: $(jqMensaje),
		css: {
			border:		'none',
			background:	'none'
			}
	});
	
	$.post( url, $(jqFormaId).serialize(), function( data ) {
		if(data.length >0) {
			$(jqMensaje).html(data);
			var exito =	$('#numeroMensaje').val();
			if(exito == 0 && funcionExito != ''){
				eval(funcionExito+ '();');
			}
		}
		
	});
	
}

function ejecutaConsulta(metodo, divSalida){
	bloqueaPantalla('contenido', 'mensaje');
	var formId 		= document.forms[0].getAttribute('id');
	var jqFormaId 	= eval("'#"+formId+"'");
	var jqSalida		= eval("'#"+divSalida+"'");
	var path 		= location.pathname;
	var method 		= path.split("/").slice(-1)[0];
	var url 		= path.replace(method, metodo + ".htm");
	$.get( url, $(jqFormaId).serialize(), function( data ) {
		if(data.length >0) {
			$(jqSalida).html(data);
			desbloqueaPantalla('contenido');
		}else{
			desbloqueaPantalla('contenido');
		}
	});
	
}

function mensajeModal(divBloquear, mensaje){
	var jqBloquear	=	eval("'#"+divBloquear+"'");
	$('#mensajeAlert').text(mensaje);
	$(jqBloquear).block({
		message: $('#mensajeModal'),
		css: {
			border:		'none',
			background:	'none'
			}
	});
}


function formatCurrency(total) {
    var neg = false;
    if(total < 0) {
        neg = true;
        total = Math.abs(total);
    }
    return (neg ? "-$" : '$') + parseFloat(total, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
}


function transaccion(formId, divMensaje, funcionExito, funcionError){
	var formaId	=	eval("'#"+formId+"'");
	var url			=	$(formaId).attr('action');
	var idMensaje	=	eval("'#"+divMensaje+"'");	
	
	$(idMensaje).html('<div align="center"><img src="imagenes/seguridad/barras.jpg" alt=""/></div>');
	$(idMensaje).modal();
	
	$.post( url, $(formaId).serialize(), function( data ) {
		if(data.length > 0) {
			$.modal.close();
			$(idMensaje).html(data);
			$(idMensaje).modal();			

			var exito =	$('#numeroMensaje').val();
			if(exito == 0 && funcionExito != ''){
				eval(funcionExito+ '();');
			}					
		}	
	});
	
}