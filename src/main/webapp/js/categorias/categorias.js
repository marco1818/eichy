$(document).ready(function(e){
	
	$('#btnAceptar').click(function(){
		grabaTransaccionRecarga('categoriasBean','contenido','mensaje','exitoAlta','');
	});
	
	$('#categoriaId').change(function(){
		bloqueaPantalla('contenido', 'mensaje');
		if($('#categoriaId').val() != '0'){
			$('#tipoOperacion').val('2');
			var categoriaBean	=	{
				'categoriaId'	: $('#categoriaId').val()
				}
			
			CategoriasServicioImpl.consulta(categoriaBean,2,function(categoria){
				$('#nombre').val(categoria.nombre);
				
			});
			desbloqueaPantalla('contenido');

		}else{
			
			$('#tipoOperacion').val('1');
			$('#nombre').val('');
			desbloqueaPantalla('contenido');

		}
	});
});

function exitoAlta(){
	consultaCombo();
	$('#tipoOperacion').val('1');
	$('#nombre').val('');
}

function	consultaCombo(){
	var categoriaBean	=	{
			}
	dwr.util.removeAllOptions('categoriaId'); 
	dwr.util.addOptions('categoriaId',{'0':'NUEVA CATEGORIA'});
	
	CategoriasServicioImpl.lista(categoriaBean,1,function(categoria){
		if(categoria.length > 0){
			dwr.util.addOptions('categoriaId', categoria, 'categoriaId','nombre');
		}else{
			
		}
	});
}
	