$(document).ready(function(e){
	
	$('#btnAceptar').click(function(){
		grabaTransaccionRecarga('productosForm','contenido','mensaje','exitoAlta','');
	});
	
	$('#productoId').change(function(){
		bloqueaPantalla('contenido', 'mensaje');
		if($('#productoId').val() !='0'	){
			$('#tipoOperacion').val('2');
			consultaProducto();
		}else{
			$('#tipoOperacion').val('1');
			limpiaProducto();
			limpiaInsumos();
			desbloqueaPantalla('contenido');
		}
	});
	
	$('#insumoId').change(function(){
		bloqueaPantalla('contenido', 'mensaje');
		
		if($('#insumoId').val() != '0'){
			consultaInsumo();
			return;
		}
		
		if($('#insumoId').val() == '0'){
			$('#precioCompraInsumo').val('');
			$('#precioVentaInsumo').val('');
			$('#unidades').val('');
			$('#seleccionable').attr('checked', false);
			desbloqueaPantalla('contenido');
			return;
		}
		
		
	});
	
	$('#btnAgregarInsumo').click(function(){
		
		if(validaInsumo()==0){
			bloqueaPantalla('contenido', 'mensaje');
			var insumo	=	"<tr id='insumoTR"+$('#insumoId').val()+"'>"
						+		"<td  align=\"center\">"
						+			"<label>"+$('#insumoId option:selected').text()+"</label>"
						+			"<input type='hidden' name='insumoAlta' value='"+$('#insumoId').val()+"'>"
						+		"</td>"
						+		"<td align=\"center\">"
						+			"<label id='precioGridInsumo"+$('#insumoId').val()+"'>"+$('#precioVentaInsumo').val()+"</label>"
						+		"</td>"
						+		"<td align=\"center\">"
						+			"<label id='precioCompraGrid"+$('#insumoId').val()+"'>"+$('#precioCompraInsumo').val()+"</label>"
						+		"</td>"
						+		"<td align=\"center\">"
						+			"<label>"+$('#unidades').val()+"</label>"
						+			"<input id='unidadesGrid"+$('#insumoId').val()+"' type='hidden' name='unidades' value='"+$('#unidades').val()+"'>"
						+		"</td>"
						+		"<td align=\"center\">"
						+			"<input type='checkbox'"+($('#seleccionable').prop('checked')?"checked='checked'":"")+" disabled='disabled' >"
						+			"<input id='seleccionableGrid"+$('#insumoId').val()+"' type='hidden' name='seleccionable' value='"+($('#seleccionable').prop('checked')?"1":"0")+"'>"
						+		"</td>"
						+		"<td align=\"center\">"
						+			"<a href=\"javascript:void(0)\" onclick=\"eliminarInsumo('"+$('#insumoId').val()+"')\">Quitar</a>"
						+		"</td>"
						+	"</tr>";	
	
			$('#tablaInsumos tbody').append(insumo);
			
			if(!$('#seleccionable').prop('checked')){
				var precioActual 	= 	isNaN(parseFloat($('#precioUnitario').val()))?0:parseFloat($('#precioUnitario').val());
				var precioInsumo	=	isNaN(parseFloat($('#precioVentaInsumo').val()))?0:parseFloat($('#precioVentaInsumo').val());
				$('#precioUnitario').val(precioActual+(precioInsumo * parseFloat($('#unidades').val())));
				var precioCompraActual = isNaN(parseFloat($('#precioCompraProducto').val()))?0:parseFloat($('#precioCompraProducto').val());
				var precioCompraInsumo = isNaN(parseFloat($('#precioCompraInsumo').val()))?0:parseFloat($('#precioCompraInsumo').val());
				$('#precioCompraProducto').val(precioCompraActual+(precioCompraInsumo*parseFloat($('#unidades').val())));
				$('#precioCompraProducto1').val(precioCompraActual+(precioCompraInsumo*parseFloat($('#unidades').val())));
				limpiaInsumoSeleccion();
			}else{
				limpiaInsumoSeleccion();
			}
			
			desbloqueaPantalla('contenido');
		}
		
			
	});
	
	function validaProducto(){
		if( $.trim($('#nombre').val()) == ''){
			alert('El nombre del producto esta vacio.');
			return false;
		}
		
		if( $('#categoriaId').val() == '0'){
			alert('No se ha seleccionado una categoria.');
			return false;
		}
		
		if( $.trim($('#precioUnitario').val()) == ''){
			alert('El precio unitario esta vacio.');
			return false;
		}
		
		if( $.trim($('#precioMayoreo').val()) == ''){
			alert('El precio mayoreo esta vacio.');
			return false;
		}
		
		if( $.trim($('#unidadesMayoreo').val()) == ''){
			alert('El numero de unidades mayoreo esta vacio.');
			return false;
		}
		
		return true;
	}
	
	function validaInsumo(){
		var errores = 0;
		if($('#insumoId').val() == '0'){
			alert('Debe seleccionar un insumo.');
			errores= 1;
			return errores;
		}
		
		$("input[name=insumoAlta]").each(function(){
            if($(this).val() == $('#insumoId').val()){
            	alert('El insumo ya se encuentra asociado.');
            	errores=1;
            	return errores;
            }
        });
		
		if($.trim($('#unidades').val()) == ''){
			alert('El numero de Unidades esta Vacio.');
			errores=1;
			return errores;
		}
		
		return errores;
	}
	
	function consultaProducto(){
		$('#nombre').val('');
		$('#categoriaId').val('0');
		$('#precioUnitario').val('');
		$('#precioMayoreo').val('');
		$('#unidadesMayoreo').val('');
		$('#precioCompraProducto').val('0');
		$('#precioCompraProducto1').val('0');
		$('#estatus').val('1');
		limpiaInsumos();
		
		var producto	=	{
				'productoId'	: $('#productoId').val()
				}

		ProductosServicioImpl.consulta(producto,2,function(productoBean){
			$('#nombre').val(productoBean.producto.nombre);
			$('#categoriaId').val(productoBean.producto.categoriaId);
			$('#precioUnitario').val(productoBean.producto.precioUnitario);
			$('#precioMayoreo').val(productoBean.producto.precioMayoreo);
			$('#unidadesMayoreo').val(productoBean.producto.unidadesMayoreo);
			$('#precioCompraProducto').val(productoBean.producto.precioCompra);
			$('#precioCompraProducto1').val(productoBean.producto.precioCompra);
			$('#estatus').val(productoBean.producto.estatus);

			var insumos	=	{
					'productoId'	: $('#productoId').val()
					}
			
			ProductosServicioImpl.consulta(insumos,5, function(insumosxProductosBean){
				
				for(var x = 0; x < insumosxProductosBean.insumosxProductos.length; x++){
					var insumo	=	"<tr id='insumoTR"+insumosxProductosBean.insumosxProductos[x].insumoId+"'>"
								+		"<td  align=\"center\">"
								+			"<label>"+insumosxProductosBean.insumosxProductos[x].descripcion+"</label>"
								+			"<input type='hidden' name='insumoAlta' value='"+insumosxProductosBean.insumosxProductos[x].insumoId+"'>"
								+		"</td>"
								+		"<td align=\"center\">"
								+			"<label id='precioGridInsumo"+insumosxProductosBean.insumosxProductos[x].insumoId+"'>"+insumosxProductosBean.insumosxProductos[x].precioVenta+"</label>"
								+		"</td>"
								+		"<td align=\"center\">"
								+			"<label id='precioCompraGrid"+insumosxProductosBean.insumosxProductos[x].insumoId+"'>"+insumosxProductosBean.insumosxProductos[x].precioCompra+"</label>"
								+		"</td>"
								+		"<td align=\"center\">"
								+			"<label>"+insumosxProductosBean.insumosxProductos[x].unidades+"</label>"
								+			"<input id='unidadesGrid"+insumosxProductosBean.insumosxProductos[x].insumoId+"' type='hidden' name='unidades' value='"+insumosxProductosBean.insumosxProductos[x].unidades+"'>"
								+		"</td>"
								+		"<td align=\"center\">"
								+			"<input type='checkbox'"+(insumosxProductosBean.insumosxProductos[x].seleccionable==1?"checked='checked'":"")+" disabled='disabled' >"
								+			"<input id='seleccionableGrid"+insumosxProductosBean.insumosxProductos[x].insumoId+"' type='hidden' name='seleccionable' value='"+insumosxProductosBean.insumosxProductos[x].seleccionable+"'>"
								+		"</td>"
								+		"<td align=\"center\">"
								+			"<a href=\"javascript:void(0)\" onclick=\"eliminarInsumo('"+insumosxProductosBean.insumosxProductos[x].insumoId+"')\">Quitar</a>"
								+		"</td>"
								+	"</tr>";	
			
					$('#tablaInsumos tbody').append(insumo);
				}
				desbloqueaPantalla('contenido');
			});
		});
	}
	
});

function consultaInsumo(){
	var insumoBean	=	{
			'insumoId'	: $('#insumoId').val()
			}
	
	InsumosServicioImpl.consulta(insumoBean,1,function(insumo){
		$('#precioVentaInsumo').val(insumo.precioVenta);
		$('#precioCompraInsumo').val(insumo.precioCompra);
		desbloqueaPantalla('contenido');
	});

}

function exitoAlta(){
	limpiaProducto();
	limpiaInsumos();
	consultaProductos();
}

function limpiaProducto(){
	$('#productoId').val('0');
	$('#nombre').val('');
	$('#categoriaId').val('0');
	$('#precioUnitario').val('');
	$('#precioMayoreo').val('');
	$('#unidadesMayoreo').val('');
	$('#precioCompraProducto').val('0');
	$('#precioCompraProducto1').val('0');
	$('#estatus').val('1');
}

function limpiaInsumoSeleccion(){
	$('#insumoId').val('0');
	$('#precioVentaInsumo').val('');
	$('#precioCompraInsumo').val('');
	$('#unidades').val('');
	$('#seleccionable').attr('checked', false);
}

function limpiaInsumos(){
	limpiaInsumoSeleccion();
	$('#tablaInsumos tbody').html('');
}

function eliminarInsumo(insumoId){

	if($('#seleccionableGrid'+insumoId).val() == '0'){
		var precioActual 	= 	isNaN(parseFloat($('#precioUnitario').val()))?0:parseFloat($('#precioUnitario').val());
		var precioInsumo	=	isNaN(parseFloat($('#precioGridInsumo'+insumoId).text()))?0:parseFloat($('#precioGridInsumo'+insumoId).text());
		$('#precioUnitario').val(precioActual-(precioInsumo*parseFloat($('#unidadesGrid'+insumoId).val())));
		
		var precioCompraActual = isNaN(parseFloat($('#precioCompraProducto').val()))?0:parseFloat($('#precioCompraProducto').val());
		var precioCompraInsumo = isNaN(parseFloat($('#precioCompraGrid'+insumoId).text()))?0:parseFloat($('#precioCompraGrid'+insumoId).text());
		$('#precioCompraProducto').val(precioCompraActual-(precioCompraInsumo*parseFloat($('#unidadesGrid'+insumoId).val())));
		$('#precioCompraProducto1').val(precioCompraActual-(precioCompraInsumo*parseFloat($('#unidadesGrid'+insumoId).val())));
	}

	$('#insumoTR'+insumoId).remove();
}

function consultaProductos(){
	
	var producto = {
			
	}
	
	dwr.util.removeAllOptions('productoId'); 
	dwr.util.addOptions('productoId',{'0':'NUEVO PRODUCTO'});
	
	ProductosServicioImpl.consulta(producto, 1, function(productoBean){
		if(productoBean.productos.length > 0){
			dwr.util.addOptions('productoId', productoBean.productos, 'productoId','nombre');
		}else{
			
		}
	});
}






