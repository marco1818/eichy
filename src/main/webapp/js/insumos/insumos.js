$(document).ready(function(e){
	$('#btnAceptar').click(function(){
		grabaTransaccionRecarga('insumosBean','contenido','mensaje','exitoAlta','');
	});
	
	$('#insumoId').change(function(){
		bloqueaPantalla('contenido', 'mensaje');
		if($('#insumoId').val() != '0'){
			$('#tipoOperacion').val('2');
			var insumoBean	=	{
				'insumoId'	: $('#insumoId').val()
				}
			
			InsumosServicioImpl.consulta(insumoBean,1,function(insumo){
				$('#descripcion').val(insumo.descripcion);
				$('#precioVenta').val(insumo.precioVenta);
				$('#tipoUnidadId').val(insumo.tipoUnidadId);
				$('#precioCompra').val(insumo.precioCompra);
				
			});
			desbloqueaPantalla('contenido');

		}else{
			
			$('#tipoOperacion').val('1');
			$('#descripcion').val('');
			$('#precioVenta').val('');
			$('#tipoUnidadId').val('');
			$('#precioCompra').val('');
			desbloqueaPantalla('contenido');

		}
	});
	


});

function exitoAlta(){
	consultaCombo();
	$('#tipoOperacion').val('1');
	$('#descripcion').val('');
	$('#precioVenta').val('');
	$('#tipoUnidadId').val('');
	$('#precioCompra').val('');
}

function	consultaCombo(){
	var insumoBean	=	{
			}
	dwr.util.removeAllOptions('insumoId'); 
	dwr.util.addOptions('insumoId',{'0':'NUEVO INSUMO'});
	
	InsumosServicioImpl.consultaLista(insumoBean,1,function(insumo){
		if(insumo.length > 0){
			dwr.util.addOptions('insumoId', insumo, 'insumoId','descripcion');
		}else{
			
		}
	});
}
	
	
	
	